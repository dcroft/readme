# README

My focus is currently split as follows: People: 60%, Product: 10%, Process: 30%

### Ways of working

Optimise for Single Source of Truth (SSoT) and Self Service to enable effective, asynchronous collaboration driving results.

Diversity, Inclusion, and Belonging is not just a work priority it's a way of relating to other people. A quote from [GitLab's Diversity, Inclusion, and Belonging page](https://about.gitlab.com/company/culture/inclusion/) that sums this up: 

> A good way to look at Diversity, Inclusion & Belonging is, Diversity is having a seat at the table, Inclusion is having a voice and feeling empowered to use it, and Belonging is acknowledgement of your voice being heard along with creating an environment where team members feel secure to be themselves.

I view software development teams as inclusive of all team members directly contributing to the group. This includes product managers, product designers, product developers, researchers, and other team members.

I want to make sure that people understand the value of metrics in the context of their work. It's a guide to evaluate our alignment with broader organisational goals and a quantifiable value that we can measure our efforts to improve.

I default to transparency, we should have [a reason for not making something public](https://about.gitlab.com/handbook/communication/#not-public).

#### Process as a product

In leadership positions, sometimes process can turn into an ever-growing list of checkboxes. We should be careful to evaluate process for efficacy and fitness for purpose. I strive to not only describe the why of process, determine the mtric by which I'm evaluating the efficacy of the process, and the expectation of the impact of the process. We should treat process the same way software engineers treat their code; there are intended outcomes, there are metrics to assess, change isn't free, I want to make sure the cost of change is worth it. Try the change, be ok with being wrong, find a better solution if you are.

#### Boundaries of excellence

We all have a role in a team, some folks have the same role, some have a unique contribution they need to make. We should define boundaries of ownership - DRI-ness in GitLab terms - around the expertise of our team members and then provide coaching and support such that those people can excel. This doesn't mean others can't contribute, but in the absence of other suggestions, or when there's contention the person with that expertise are responsible for making a call.

#### Ownership in the absence of ownership

One of the unique aspects of leadership roles compared to other folks in teams is that we need to be ready to take ownership in the absence of some clear owner or expertise boundary. That can mean that when someone's out, we need to make the call. It can mean that when it isn't clear who should be responsible, we need to make that call. This doesn't mean we're perfectly able to replicate other people's expertise or that every decision made needs to be correct, just that we need to provide the framework to allow our teams to move forward.

#### Local optimums

I believe in local optimums for teams, each team is made up of unique individuals with unique strengths and weaknesses. As such the way of working for that team may differ from another team. By providing clear goals and measures of success, leaders should optimise to achive those goals. 

#### Alignment is critical

We must dive alignment, it's critical to our organisation and team that we're working towards key comapny goals. This requires clear, concise communication of the goals, how we're measured against them, and how our contribution is critical to their success.

### Management Style

Empathetic leadership and building high performing teams that deliver business goals are not only compatible but are ideal when in unison.

I strive to embody the ideas in ["Turn the Ship Around" by "David L Marquet"](http://davidmarquet-com.3dcartstores.com/Autographed-Book-Hardcover-Turn-the-Ship-Around-_p_13.html). I believe in empowering people with information to make decisions.

https://www.youtube.com/watch?v=OqmdLcyES_Q  
https://www.youtube.com/watch?v=ivwKQqf4ixA

Thinking in terms of motivating folks, I want to empower/give autonomy, coach in the postive to build mastery, and explain the purpose of what we're doing even when it's not clear. These ideals line up with Daniel Pink's core tenents of motivation described in [the book "Drive"](https://www.danpink.com/books/drive/).

https://www.youtube.com/watch?v=u6XAPnuFjJc

What does "coach in the positive" mean to me? It means that I can help people build a set of positive outcomes and scenarios by explaining how decisions/actions are good and why. Additionally, talking through things we could have done better build a more complete set of experiences to make future decisions around.

I don't own other people's failures as I don't own other people's successes.

I lead from behind - I would prefer to help someone succeed through personal guidance than publicly correct them. I recognise my role requires me to help people succeed while enabling a strong sense of ownership. I avoid micromanagement and prioritise building a trusting, collaborative relationship with my peers and team.

I continuously drive towards action; I have often struggled with this, so I empathise with people who also struggle and know that sometimes a friendly nudge is all someone needs to excel.

Empathy and vulnerability build trust, I am fallible as are others. 

### This Page

Will be a work in progress always. :-)

### Updates

- 2023-02-10
- 2022-05-16
