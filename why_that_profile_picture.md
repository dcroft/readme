### What's the story?

![Portrait](https://gitlab.com/dcroft/readme/-/raw/master/images/portrait_small.jpg "me holding a goat, wearing victorian era clothing, and looking majestically off into the distance")

So, several years ago when I was living in Southern California, a friend of ours was studying fine art. She'd made a portrait of another friend of ours that was pretty amazing and asked if other folks were interested because she was looking to practice. Being that I have an, at times, very silly sense of humour, and chatting with this friend; we decided on the portrait you see above. It's me, staring majestically into the distance, in Victorian era clothing, holding a baby goat.

This friend and I worked back and forth on the concept and she sent me update pictures during the painting of the piece. Once it was complete, I got the final picture from her and then she shipped it to me. The thing is, I never opened the package when it arrived.

I hatched a plan to ship the portrait to my best friend Chris who lives in Boston with his family. So, when it arrived, I went to the UPS and shipped it directly him. I sent him a message with the tracking number. He asked what it was and I just said "you'll find out".

It's now hanging in Chris' house I'm sure bringing some hilarious questions and conversations. I've yet to see it in person, I guess I should plan a visit.
